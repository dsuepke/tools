#!/usr/bin/env python3

import glob
import os
import shutil
import sys

COLOR_NORMAL = '\033[0m'
COLOR_ERROR = '\033[91m'
COLOR_WARN = '\033[93m'
COLOR_INFO = '\033[96m'
COLOR_GOOD = '\033[92m'

if shutil.which("gs") is None:
	print(COLOR_ERROR + 'ERROR: gs (GhostScript) not found, please install first')
	sys.exit(1)

PDF_FOLDER_SRC = '/Users/dsuepke/Documents/Scans'
PDF_FOLDER_DST = '/Users/dsuepke/Documents/Scans/Shrunk'

print('Input folder:  ' + PDF_FOLDER_SRC)
print('Output folder: ' + PDF_FOLDER_DST)

if not os.path.exists(PDF_FOLDER_SRC):
	print(COLOR_WARN + "WARN: " + PDF_FOLDER_SRC + " does not exist, creating..." + COLOR_NORMAL)
	os.makedirs(PDF_FOLDER_SRC)
if not os.path.exists(PDF_FOLDER_DST):
	print(COLOR_WARN + "WARN: " + PDF_FOLDER_DST + " does not exist, creating..." + COLOR_NORMAL)
	os.makedirs(PDF_FOLDER_DST)

pdf_files_all = [os.path.basename(x) for x in glob.glob(PDF_FOLDER_SRC + '/*.pdf')]
pdf_files_done = [os.path.basename(x) for x in glob.glob(PDF_FOLDER_DST + '/*.pdf')]
pdf_files_new = list(set(pdf_files_all).difference(pdf_files_done))

print('\nFound ' + str(len(pdf_files_all)) + ' PDFs, ' + COLOR_GOOD + str(len(pdf_files_new)) + ' new' + COLOR_NORMAL + ' and ' + (str(len(pdf_files_all) - len(pdf_files_new))) + ' finished.')

if len(pdf_files_new) == 0:
	sys.exit(0)

size_before = 0
size_after = 0

for pdf in pdf_files_new:
	pdf_in = PDF_FOLDER_SRC + '/' + pdf
	pdf_out = PDF_FOLDER_DST + '/' + pdf
	os.system(
		'gs -dNOPAUSE -dBATCH -dPrinted=false -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -sOutputFile="' + pdf_out + '" "' + pdf_in + '" &> /dev/null')

	pdf_size_in = os.path.getsize(pdf_in) / 1024
	pdf_size_out = os.path.getsize(pdf_out) / 1024
	pdf_size_diff = 100 - (pdf_size_out / pdf_size_in * 100)
	print('Shrank {} ({}k → {}k): {}%'.format(
		pdf, str(int(pdf_size_in)), str(int(pdf_size_out)), str(int(pdf_size_diff))))

	size_before += pdf_size_in
	size_after += pdf_size_out

if (len(pdf_files_new)) > 1:
	size_diff = 100 - (size_after / size_before * 100)
	print('Finished shrinking. Reduced overall size ({}k → {}k): {}%'.format(
		str(int(size_before)), str(int(size_after)), str(int(size_diff))))
